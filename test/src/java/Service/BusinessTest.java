/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.java.Service;

import InterfaceImpl.Female;
import InterfaceImpl.LowerAgeGroup;
import InterfaceImpl.Male;
import InterfaceImpl.MiddleAgeGroup;
import InterfaceImpl.Other;
import InterfaceImpl.UpperAgeGroup;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import src.Interfaces.Age;
import src.Interfaces.Gender;
import src.java.Dao.HibernateDao;
import src.java.Entity.Client;
import src.java.Entity.Habit;
import src.java.Entity.Health;
import src.java.Utils.ConstantsUtil;
import src.java.Utils.StaticUtils;

/**
 *
 * @author Nikhil
 */
public class BusinessTest {
  Business objBusiness=new Business();
  Client objClient=new Client();
  Habit objHabit=new Habit();
  Health objHealth=new Health();
  Gender genderObj=null;
  Age ageObj=null;
  double amount=ConstantsUtil.BASE_PREMIUM;

  
// Result Check
    @Test
    public void testServices() {
        System.out.println("services Test");
        objClient.setName("Norman Gomes");
        objClient.gender="male".toLowerCase();
        objClient.age=34;
        objHealth.setBlood_Pressure("no");
        objHealth.setBlood_Sugar("no");
        objHealth.setHypertension("no");
        objHealth.setOverweight("yes");
        objHabit.setAlcohol("yes");
        objHabit.setDaily_Exercise("Yes");
        objHabit.setDrugs("no");
        objHabit.setSmoking("no");
          switch(objClient.gender)
           {
            case "male": genderObj=new Male();break;
            case "female": genderObj=new Female();break;
            case "other": genderObj=new Other();break;
            }
            amount=genderObj.genderBasedCalculation(amount);

           if(objClient.age<18){
               ageObj=new LowerAgeGroup();
                     }
             else if(objClient.age>=18 && objClient.age<40){
                ageObj=new MiddleAgeGroup();
                }
              else{
               ageObj=new UpperAgeGroup();
                   }
             amount=ageObj.calculatePremiumByAge(amount, objClient.age);
             amount=objHealth.checkHealthConditions(objHealth, amount);
             amount=objHabit.checkHabits(objHabit, amount);
             objClient.finalAmount=Math.ceil(amount);
             double expResult = 6856;
             assertEquals(expResult, Math.round(amount),0.0);

    }
  @Test
    public void testLowerAgeGroup() {
        System.out.println("testLowerAgeGroup Test");
        int age=17;
        double expResult = 0.0;
        ageObj=new LowerAgeGroup();
        double result = ageObj.calculatePremiumByAge(amount, age);
        assertEquals(5000, result, 0.0);
  }
   
    @Test
    public void testUpperAgeGroup() {
        System.out.println("testUpperAgeGroup Test");
        int age=34;
        double expResult = 6856.0;
        ageObj=new UpperAgeGroup();
        double result = ageObj.calculatePremiumByAge(amount, age);
        assertEquals(6655, result, 0.0);
        
    }

    @Test
    public void testMiddleAgeGroup() {
        System.out.println("testMiddleAgeGroup Test");
           int age=19;
        double expResult = 0.0;
        ageObj=new MiddleAgeGroup();
        double result = ageObj.calculatePremiumByAge(amount, age);
        assertEquals(5500, result, 0.0);
       
    }
    @Test
    public void testFemaleGenderPerc() {
    System.out.println("testFemaleGenderPerc Test");
    genderObj=new Female();
    double result=genderObj.genderBasedCalculation(amount);
    double expResult = 5000.0;
    assertEquals(expResult, result, 0.0);
  }
     @Test
    public void testMaleGenderPerc() {
    System.out.println("testMaleGenderPerc Test");
    genderObj=new Male();
    double result=genderObj.genderBasedCalculation(amount);
    double expResult = 5100.0;
    assertEquals(expResult, result, 0.0);
  }
     @Test
    public void testOtherGenderPerc() {
    System.out.println("testOtherGenderPerc Test ");
    genderObj=new Other();
    double result=genderObj.genderBasedCalculation(amount);
    double expResult = 5000.0;
    assertEquals(expResult, result, 0.0);
  }

    @Test
    public void testIncrement() {
        System.out.println("incrementStep Test ");
        int step = 2;
        int value = 6;
        int expResult = 8;
        int result = StaticUtils.increment(step, value);
        assertEquals(expResult, result);
    }

   @Test
    public void testCheckHealthConditions() {
        System.out.println("checkHealthConditions test");
        objHealth.setBlood_Pressure("no");
    objHealth.setBlood_Sugar("no");
    objHealth.setHypertension("no");
    objHealth.setOverweight("yes");
    double expResult = 5050.0;
    double result = objHealth.checkHealthConditions(objHealth, amount);
    assertEquals(expResult, result, 0.0);
    }

   @Test
    public void testCheckHabits() {
        System.out.println("checkHabits Test");
        double expResult = 5150.0;
        objHabit.setAlcohol("yes");
        objHabit.setDaily_Exercise("yes");
        objHabit.setDrugs("no");
        objHabit.setSmoking("yes");
        double result = objHabit.checkHabits(objHabit, amount);
        assertEquals(expResult, result, 0.0);
    }


    
}
