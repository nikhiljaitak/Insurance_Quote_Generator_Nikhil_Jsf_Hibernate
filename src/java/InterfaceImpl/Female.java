/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceImpl;

import src.Interfaces.Age;
import src.Interfaces.Gender;
import src.java.Utils.ConstantsUtil;
import src.java.Utils.StaticUtils;

/**
 *
 * @author Nikhil
 */
public class Female implements Gender{

    @Override
    public double genderBasedCalculation(double amount) {
   return StaticUtils.calculatePercentage(amount,ConstantsUtil.GENDER_OTHER_PERCENTAGE);
    }

    

    
}
