/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InterfaceImpl;

import src.Interfaces.Age;
import src.java.Utils.ConstantsUtil;
import src.java.Utils.StaticUtils;
import static src.java.Utils.StaticUtils.calculatePercentage;

/**
 *
 * @author Nikhil
 */
public class UpperAgeGroup implements Age{

    @Override
    public double calculatePremiumByAge(double amount, int age) {
    int percentage=ConstantsUtil.BELOW_40_PERCENTAGE;
    int a=25;// start Pointer
    int b=30; // End pointer
    double calculatedAmount=calculatePercentage(amount,percentage);
    while(a<=age)
      {
       if(a>=40){
          percentage=ConstantsUtil.ABOVE_40_PERCENTAGE;
          }
       calculatedAmount=calculatePercentage(calculatedAmount, percentage);
       a=StaticUtils.increment(a,5);
       b=StaticUtils.increment(b,5);
      }
    return calculatedAmount;}
}
