package src.java.Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import src.java.Entity.Client;
import src.java.Utils.ConstantsUtil;
import src.java.Entity.Habit;
import src.java.Entity.Health;
import java.util.Scanner;
import org.apache.log4j.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import src.java.Dao.HibernateDao;
import src.java.Service.Business;


/**
 *
 * @author Nikhil
 */
@ManagedBean(name="runner")
@SessionScoped
public class Runner {
    private static final Logger logger = Logger.getLogger(Runner.class);
@ManagedProperty("#{client}")
public Client client;
@ManagedProperty("#{health}")
public Health health;
@ManagedProperty("#{habits}")
public Habit  habits;
public double finalAmount;
@ManagedProperty("#{business}")
private Business business;




    public Business getBusiness() {
        return business;
    }

    public void setBusiness(Business business) {
        this.business = business;
    }


    public double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(double finalAmount) {
        this.finalAmount = finalAmount;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Health getHealth() {
        return health;
    }

    public void setHealth(Health health) {
        this.health = health;
    }

    public Habit getHabits() {
        return habits;
    }

    public void setHabits(Habit habits) {
        this.habits = habits;
    }

  public String frontController()
  {
   if(client!=null && habits!=null && health!=null){
       //logger.error("This is Error message", new Exception("Testing"));
   business.services();
 }
   else{
   return "failure";
   }
  
  return "output"; }
    
}
