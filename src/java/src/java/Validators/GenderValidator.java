/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.java.Validators;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Nikhil
 */
@ManagedBean
@SessionScoped
@FacesValidator("genderValidator")
public class GenderValidator implements Validator{
     @Override
    public void validate(FacesContext context, UIComponent comp, Object value) throws ValidatorException {
      String gender=(String)value;
       if(gender==null)
    {
       
FacesContext.getCurrentInstance().getPartialViewContext();

   FacesMessage message = new FacesMessage(
					"Please Enter Gender");
			context.addMessage(comp.getClientId(context), message); 
    
    throw new ValidatorException(message);
    }
    }
}
