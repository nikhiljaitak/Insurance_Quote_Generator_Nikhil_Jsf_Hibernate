/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.java.Entity;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import src.java.Utils.ConstantsUtil;
import src.java.Utils.StaticUtils;

/**
 *
 * @author Nikhil
 */
@ManagedBean(name="health")
@SessionScoped
@Embeddable
public class Health implements Serializable{
    @Column(name="hypertension")
    public String hypertension;
    @Column(name="blood_Pressure")
     public String blood_Pressure;
    @Column(name="blood_Sugar")
    public String blood_Sugar;
    @Column(name="overweight")
    public String overweight;
    
    public Health() {
        
    }
    public double checkHealthConditions(Health conditions, double amount) {
        int percentage=0;
      if(conditions.getBlood_Pressure().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getBlood_Sugar().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getHypertension().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
        if(conditions.getOverweight().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
        }
    return StaticUtils.calculatePercentage(amount, percentage);
    }
    public String getHypertension() {
        return hypertension;
    }

    public void setHypertension(String hypertension) {
        this.hypertension = hypertension;
    }

    public String getBlood_Pressure() {
        return blood_Pressure;
    }

    public void setBlood_Pressure(String blood_Pressure) {
        this.blood_Pressure = blood_Pressure;
    }

    public String getBlood_Sugar() {
        return blood_Sugar;
    }

    public void setBlood_Sugar(String blood_Sugar) {
        this.blood_Sugar = blood_Sugar;
    }

    public String getOverweight() {
        return overweight;
    }

    public void setOverweight(String overweight) {
        this.overweight = overweight;
    }
    
}
