/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.java.Entity;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import src.java.Utils.ConstantsUtil;
import src.java.Utils.StaticUtils;

/**
 *
 * @author Nikhil
 */
@ManagedBean(name="habits")
@SessionScoped
@Embeddable
public class Habit implements Serializable{
    @Column(name="smoking")
public String smoking;
    @Column(name="alcohol")
public String alcohol;
    @Column(name="daily_Exercise")
public String daily_Exercise;
@Column(name="drugs")
public String drugs;

public Habit(String smoking, String alcohol, String daily_Exercise, String drugs) {
        this.smoking = smoking;
        this.alcohol = alcohol;
        this.daily_Exercise = daily_Exercise;
        this.drugs = drugs;
    }

    public Habit() {
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(String alcohol) {
        this.alcohol = alcohol;
    }

    public String getDaily_Exercise() {
        return daily_Exercise;
    }

    public void setDaily_Exercise(String daily_Exercise) {
        this.daily_Exercise = daily_Exercise;
    }

    public String getDrugs() {
        return drugs;
    }

    public void setDrugs(String drugs) {
        this.drugs = drugs;
    }
    
     public double checkHabits(Habit conditions, double amount) {
         int percentage=0;
        if(conditions.getAlcohol().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
        }
        if(conditions.getDaily_Exercise().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_GOOD_PERCENTAGE;
        }
        if(conditions.getDrugs().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
        }
        if(conditions.getSmoking().equalsIgnoreCase(ConstantsUtil.YES)){
        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
        }
         return StaticUtils.calculatePercentage(amount, percentage);
    }

}
