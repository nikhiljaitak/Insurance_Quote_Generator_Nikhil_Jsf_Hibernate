/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.java.Dao;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GeneratorType;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import src.java.Entity.Client;
import src.java.Entity.Habit;
import src.java.Entity.Health;
import src.java.Service.Business;
import src.java.Utils.HibernateUtil;

/**
 *
 * @author Nikhil
 */
@ManagedBean(name="hibernateDao")
@SessionScoped
@Entity
@Table(name="hibernates")
public class HibernateDao {
@Cascade(CascadeType.ALL)
@GeneratedValue(strategy = GenerationType.AUTO)
@Id
public int clientId;

@ManagedProperty("#{client}")
@Embedded
public Client client;
@Embedded
@ManagedProperty("#{health}")
public Health health;
@Embedded
@ManagedProperty("#{habits}")
public Habit  habits;

    public HibernateDao(Client client, Health health, Habit habits) {
        this.client = client;
        this.health = health;
        this.habits = habits;
    }


    public HibernateDao() {
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Health getHealth() {
        return health;
    }

    public void setHealth(Health health) {
        this.health = health;
    }

    public Habit getHabits() {
        return habits;
    }

    public void setHabits(Habit habits) {
        this.habits = habits;
    }

public void dataInsert()
{
     Configuration configuration = new Configuration().configure();
     configuration.addResource("hibernate.cfg.xml");
StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
applySettings(configuration.getProperties());
 SessionFactory sessionFactory= configuration.buildSessionFactory(builder.build());
    //SessionFactory session=HibernateUtil.getSessionFactory();
    Session s=sessionFactory.openSession();
    s.beginTransaction();
   HibernateDao h=new HibernateDao(client,health,habits);
    s.save(h);
    s.getTransaction().commit();
    s.clear();
    s.flush();
    s.close();
    
}
}
