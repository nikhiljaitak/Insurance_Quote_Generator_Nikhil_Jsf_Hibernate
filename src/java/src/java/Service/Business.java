/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.java.Service;

import InterfaceImpl.Female;
import InterfaceImpl.LowerAgeGroup;
import InterfaceImpl.Male;
import InterfaceImpl.MiddleAgeGroup;
import InterfaceImpl.Other;
import InterfaceImpl.UpperAgeGroup;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import src.Interfaces.Age;
import src.Interfaces.Gender;
import src.java.Dao.HibernateDao;
import src.java.Controller.Runner;
import src.java.Entity.Client;
import src.java.Entity.Habit;
import src.java.Entity.Health;
import src.java.Utils.ConstantsUtil;

/**
 *
 * @author Nikhil
 */
@ManagedBean(name="business")
@SessionScoped
public class Business{
    
@ManagedProperty("#{client}")
public Client client;
@ManagedProperty("#{health}")
public Health health;
@ManagedProperty("#{habits}")
public Habit  habits;
@ManagedProperty("#{hibernateDao}")
public HibernateDao hibernateDao;
private double finalAmount;

private static int base_Premium=5000;
/**
     * @param args the command line arguments
     */
    public double calculatePercentage(double amount,int percentage){

    return (amount+((percentage*amount)/100));
}
public void services()
{
    Age ageObj=null;
int age=client.age;
double amount=ConstantsUtil.BASE_PREMIUM;// BaseAmount
System.out.println( "amount is"+amount);
String gender=client.gender;
System.out.println( "gender is"+gender);
Gender genderObj=null;
switch(gender)
{
    case "Male": genderObj=new Male();break;
    case "Female": genderObj=new Female();break;
    case "Other": genderObj=new Other();break;
}
System.out.println( "gender obj  is"+genderObj);
amount=genderObj.genderBasedCalculation(amount);
System.out.println( "amount is"+amount);

System.out.println( "client age is"+age);
if(age<18)
{
ageObj=new LowerAgeGroup();
}
else if(age>=18 && age<40)
{
ageObj=new MiddleAgeGroup();
}
else{
ageObj=new UpperAgeGroup();
}

amount=ageObj.calculatePremiumByAge(amount, age);
System.out.println( "amount after age is"+amount);
amount=health.checkHealthConditions(health, amount);
System.out.println( "health amount is"+amount);
amount=habits.checkHabits(habits, amount);
System.out.println( "amounthabit is"+amount);
client.finalAmount=Math.ceil(amount);
//
//   amount=genderBased(amount,client.gender);
//   amount=calculateBasePremium(amount,client.age);

   

//  
// 
//   amount=pre_Existing_Health_Conditions(health, amount);
//   amount=pre_Existing_Habit_Conditions(habits, amount);
//   client.finalAmount=Math.ceil(amount);
}

//   public double pre_Existing_Health_Conditions(Health h,double amount){
//       Health conditions=h;
//       int percentage=checkHealthConditions(conditions,amount);
//       return calculatePercentage(amount, percentage);
//   }
//   public double pre_Existing_Habit_Conditions(Habit h,double amount){
//       Habit conditions=h;
//       int percentage=checkHabits(conditions,amount);
//       return calculatePercentage(amount, percentage);
//   }


//    public int checkHealthConditions(Health conditions, double amount) {
//        int percentage=0;
//      if(conditions.getBlood_Pressure().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
//        }
//        if(conditions.getBlood_Sugar().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
//        }
//        if(conditions.getHypertension().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
//        }
//        if(conditions.getOverweight().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HEALTH_CONDITION_PERCENTAGE;
//        }
//    return percentage ;
//    }

//    public int checkHabits(Habit conditions, double amount) {
//         int percentage=0;
//        if(conditions.getAlcohol().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
//        }
//        if(conditions.getDaily_Exercise().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HABIT_GOOD_PERCENTAGE;
//        }
//        if(conditions.getDrugs().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
//        }
//        if(conditions.getSmoking().equalsIgnoreCase(ConstantsUtil.YES)){
//        percentage+=ConstantsUtil.HABIT_BAD_PERCENTAGE;
//        }
//        return percentage;
//    }
     public HibernateDao getHibernateDao() {
        return hibernateDao;
    }

    public void setHibernateDao(HibernateDao hibernateDao) {
        this.hibernateDao = hibernateDao;
    }




    public Habit getHabits() {
        return habits;
    }

    public void setHabits(Habit habits) {
        this.habits = habits;
    }

    public double getFinalAmount() {
        return finalAmount;
    }
    
    public void setFinalAmount(double finalAmount) {
        this.finalAmount = finalAmount;
    }
   
    public Business() {
       
    }

 public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }


 

    public Health getHealth() {
        return health;
    }

    public void setHealth(Health health) {
        this.health = health;
    }

//    @Override
//    public double calculateBasePremium(double amount, int age) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public double genderBased(double amount, String gender) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public int increment(int step, int value) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
}
