/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src.java.Utils;

/**
 *
 * @author Nikhil
 */
public class ConstantsUtil {
    static public final int BASE_PREMIUM=5000;
    static public final String MALE="Male";
    static public final String FEMALE="Female";
    static public final String OTHER="Other";
    static public final String YES="Yes";
    static public final String NO="No";
    static public final int GENDER_MALE_PERCENTAGE=2;
    static public final int GENDER_FEMALE_PERCENTAGE=0;
    static public final int GENDER_OTHER_PERCENTAGE=0;
    static public final int ABOVE_40_PERCENTAGE=20;
    static public final int BELOW_40_PERCENTAGE=10;
    static public final int HEALTH_CONDITION_PERCENTAGE=1;
    static public final int HABIT_GOOD_PERCENTAGE=-3;
    static public final int HABIT_BAD_PERCENTAGE=3;
            
}
